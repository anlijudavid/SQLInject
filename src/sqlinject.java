
/**
 * Deteccion de credenciales falsas o con posible ataque de injeccion
 *
 * @author julian
 */
public class sqlinject {

    private static final String inj[] = {
        "' or 1=1 or \"='",
        "' or 1=1--",
        "' or 1=1#",
        "' or 1=1/*",
        "') or '1'='1--",
        "') or ('1'='1--",
        "' or 1=1--",
        "or 1=1--",
        "or 1=1",
        "' or 1=1 or '"
    };

    public static void main(String[] args) {
        System.out.println((Validar("anlijudavid@hotmail.com",
                "or 1=1-- 1=1--"
        ) ? "Los datos son validos" : "Incorrecto"));
    }

    public static boolean Validar(String email, String clave) {
        if ((email = email.trim()).length() > 0
                && (clave = clave.trim()).trim().length() > 0) {
            String aux_email = RegularEspacios(email);
            String aux_clave = RegularEspacios(clave);
            for (String inj1 : inj) {
                if (inj1.equals(aux_email)
                        || inj1.equals(Comilla_to_simple(aux_email))
                        || inj1.equals(aux_clave)
                        || inj1.equals(Comilla_to_simple(aux_clave))
                        || email.contains(inj1)
                        || clave.contains(inj1)) {
                    System.out.println("El email o clave contienen injeccion sql");
                    return false;
                }
            }
        }

        return ValidacionEmail(email);
    }

    private static String Comilla_to_simple(String sql_) {
        return sql_.replaceAll("'", "\"");
    }

    private static String RegularEspacios(String sql) {
        String aux_sql = "";
        sql = sql.trim();
        for (int i = 0; i < sql.length(); i++) {
            if (sql.charAt(i) == ' ' && sql.charAt(i) == sql.charAt(i + 1)) {
                continue;
            }
            aux_sql += sql.charAt(i);
        }
        return aux_sql;
    }

    public static boolean ValidacionEmail(String input) {
        input = input.trim();
        //matches devuelve un true o false si se cumple el patron dado en el parametro
        if (!input.matches("[-\\w\\.]+@\\w+\\.\\w+")) {
            System.out.println("No sirve ese correo");
            return false;
        }

        // comprueba que no empieze por www.        
        if (input.matches("^www.")) {
            System.out.println("Los emails no empiezan por www.");
            return false;
        }

        // comprueba que no contenga caracteres prohibidos	
        if (input.matches("[^A-Za-z0-9.@_-~#]+")) {
            System.out.println("La cadena contiene caracteres especiales");
            return false;
        }

        return true;
    }
}
